package com.test.bloodbank.serviceImpl;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.bloodbank.dto.AddCampDto;
import com.test.bloodbank.model.BloodBank;
import com.test.bloodbank.model.BloodCamps;
import com.test.bloodbank.repository.BloodBankRepository;
import com.test.bloodbank.repository.BloodCampsRepository;
import com.test.bloodbank.service.BloodCampsService;
import com.test.bloodbank.util.UtilConstants;

@Service
@Transactional
public class BloodCampsServiceImpl implements BloodCampsService{
	private static final Logger LOGGER = LoggerFactory.getLogger(BloodCampsService.class);
	
	@Autowired
	private BloodCampsRepository bloodCampRepository;
	
	@Autowired
	private BloodBankRepository bankRepository;
	
	public static Logger getLogger() {
		return LOGGER;
	}

	@Override
	public BloodCamps addCamp(AddCampDto campDto) {
        DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        Date output = Date.valueOf(outputFormatter.format(campDto.getDate())); 
		BloodCamps bloodCamp = new BloodCamps();
		BloodBank bloodBank = new BloodBank();
		bloodBank = bankRepository.findByBloodBankName(campDto.getBloodBankName()); 
		bloodCamp.setBloodBank(bloodBank);
		bloodCamp.setCampName(campDto.getCampName());
		bloodCamp.setDate(output);
		bloodCamp.setStartTime(Time.valueOf(campDto.getStartTime()));
		bloodCamp.setEndTime(Time.valueOf(campDto.getStartTime()));
		bloodCamp.setDistrict(campDto.getDistrict());
		bloodCamp.setState(campDto.getState());
		bloodCamp.setVenue(campDto.getVenue());
		bloodCamp.setTarget(campDto.getTarget());
		bloodCamp.setStatus(UtilConstants.DONATION_STATUS_PENDING);
		
		return bloodCampRepository.saveAndFlush(bloodCamp);
	}
	
	@Override
	public BloodCamps approveBloodCamp(Long bloodCampId) {
		BloodCamps bloodCamp = new BloodCamps();
		bloodCamp = bloodCampRepository.getReferenceById(bloodCampId);
		bloodCamp.setStatus(UtilConstants.BLOOD_CAMP_APPROVED);
		bloodCampRepository.saveAndFlush(bloodCamp);
		return bloodCamp;
	}
	
	@Override
	public List<BloodCamps> listAllCamps(){
		return bloodCampRepository.findAll();
	}
	
	@Override
	public BloodCamps displaySpecificCamp(Long campId) {
		return bloodCampRepository.findByCampId(campId);
	}
	
	

}
