package com.test.bloodbank.exceptions;

public class EarlyDonationException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public EarlyDonationException(String message) {
		super(message);
	}
}
