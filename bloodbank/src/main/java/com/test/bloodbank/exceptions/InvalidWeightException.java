package com.test.bloodbank.exceptions;

public class InvalidWeightException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public InvalidWeightException(String message) {
		super(message);
	}
}
