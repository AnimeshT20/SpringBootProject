package com.test.bloodbank.exceptions;

public class UserExistException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public UserExistException(String msg) {
		// TODO Auto-generated constructor stub
		super(msg);
	}
	
}
