package com.test.bloodbank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class BloodBank {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bloodBankId;
	private String bloodBankName;
	@Size(min = 10, max = 10)
	private Long contact;
	@Email(message = "Please enter a valid e-mail address")
	private String email;
	private String state;
	private String district;
	private String address;
	private Long pincode;
	private Long aPlus=0L;
	private Long oPlus=0L;
	private Long bPlus=0L;
	private Long abPlus=0L;
	private Long aMinus=0L;
	private Long oMinus=0L;
	private Long bMinus=0L;
	private Long abMinus=0L;
	@OneToOne(mappedBy = "bloodBank")
	private BloodCamps bloodCamp;
	
	@OneToOne(mappedBy = "bloodBank")
	private Appointment appointment;
	
	public BloodBank() {}
	
	public BloodBank(String bloodBankName, Long contact, String email, String state, String district,
			String address, Long pincode, Long a_Plus, Long o_Plus, Long b_Plus, Long ab_Plus, Long a_minus,
			Long o_minus, Long b_minus, Long ab_minus) {
		this.bloodBankName = bloodBankName;
		this.contact = contact;
		this.email = email;
		this.state = state;
		this.district = district;
		this.address = address;
		this.pincode = pincode;
		this.aPlus = a_Plus;
		this.oPlus = o_Plus;
		this.bPlus = b_Plus;
		this.abPlus = ab_Plus;
		this.aMinus = a_minus;
		this.oMinus = o_minus;
		this.bMinus = b_minus;
		this.abMinus = ab_minus;
	}
	public BloodBank(Long a_Plus, Long o_Plus, Long b_Plus, Long ab_Plus, Long a_minus,
			Long o_minus, Long b_minus, Long ab_minus) {
		this.aPlus = a_Plus;
		this.oPlus = o_Plus;
		this.bPlus = b_Plus;
		this.abPlus = ab_Plus;
		this.aMinus = a_minus;
		this.oMinus = o_minus;
		this.bMinus = b_minus;
		this.abMinus = ab_minus;
	}
	
	public Long getBloodBankId() {
		return bloodBankId;
	}
	public void setBloodBankId(Long bloodBankId) {
		this.bloodBankId = bloodBankId;
	}
	public String getBloodBankName() {
		return bloodBankName;
	}
	public void setBloodBankName(String bloodBankName) {
		this.bloodBankName = bloodBankName;
	}
	public Long getContact() {
		return contact;
	}
	public void setContact(Long contact) {
		this.contact = contact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public Long getaPlus() {
		return aPlus;
	}

	public void setaPlus(Long aPlus) {
		this.aPlus = aPlus;
	}

	public Long getoPlus() {
		return oPlus;
	}

	public void setoPlus(Long oPlus) {
		this.oPlus = oPlus;
	}

	public Long getbPlus() {
		return bPlus;
	}

	public void setbPlus(Long bPlus) {
		this.bPlus = bPlus;
	}

	public Long getAbPlus() {
		return abPlus;
	}

	public void setAbPlus(Long abPlus) {
		this.abPlus = abPlus;
	}

	public Long getaMinus() {
		return aMinus;
	}

	public void setaMinus(Long aMinus) {
		this.aMinus = aMinus;
	}

	public Long getoMinus() {
		return oMinus;
	}

	public void setoMinus(Long oMinus) {
		this.oMinus = oMinus;
	}

	public Long getbMinus() {
		return bMinus;
	}

	public void setbMinus(Long bMinus) {
		this.bMinus = bMinus;
	}

	public Long getAbMinus() {
		return abMinus;
	}

	public void setAbMinus(Long abMinus) {
		this.abMinus = abMinus;
	}

	public BloodCamps getBloodCamp() {
		return bloodCamp;
	}
	public void setBloodCamp(BloodCamps bloodCamp) {
		this.bloodCamp = bloodCamp;
	}
	public Appointment getAppointment() {
		return appointment;
	}
	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}
	

	

}
