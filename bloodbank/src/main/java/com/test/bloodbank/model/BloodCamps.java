package com.test.bloodbank.model;

import java.util.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class BloodCamps {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long campId;
	private String campName;
	private Date date;
	private Date startTime;
	private Date endTime;
	private String venue;
	private String state;
	private String district;
	private String status;
	private Integer target;
	
	private Long aPlus=0L;
	private Long oPlus=0L;
	private Long bPlus=0L;
	private Long abPlus=0L;
	private Long aMinus=0L;
	private Long oMinus=0L;
	private Long bMinus=0L;
	private Long abMinus=0L;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name= "bloodBankId", referencedColumnName="bloodBankId")
	private BloodBank bloodBank;
	
	public BloodCamps() {}

	public BloodCamps(Long campId, String campName, Date date, Date startTime, Date endTime, String venue, String state,
			String district, String status, Integer target, Long a_Plus, Long o_Plus, Long b_Plus, Long ab_Plus,
			Long a_minus, Long o_minus, Long b_minus, Long ab_minus, BloodBank bloodBank) {
		super();
		this.campId = campId;
		this.campName = campName;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.venue = venue;
		this.state = state;
		this.district = district;
		this.status = status;
		this.target = target;
		this.aPlus = a_Plus;
		this.oPlus = o_Plus;
		this.bPlus = b_Plus;
		this.abPlus = ab_Plus;
		this.aMinus = a_minus;
		this.oMinus = o_minus;
		this.bMinus = b_minus;
		this.abMinus = ab_minus;
		this.bloodBank = bloodBank;
	}

	public Long getCampId() {
		return campId;
	}
	public void setCampId(Long campId) {
		this.campId = campId;
	}
	
	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public BloodBank getBloodBank() {
		return bloodBank;
	}
	public void setBloodBank(BloodBank bloodBank) {
		this.bloodBank = bloodBank;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTarget() {
		return target;
	}
	public void setTarget(Integer target) {
		this.target = target;
	}

	public Long getaPlus() {
		return aPlus;
	}

	public void setaPlus(Long aPlus) {
		this.aPlus = aPlus;
	}

	public Long getoPlus() {
		return oPlus;
	}

	public void setoPlus(Long oPlus) {
		this.oPlus = oPlus;
	}

	public Long getbPlus() {
		return bPlus;
	}

	public void setbPlus(Long bPlus) {
		this.bPlus = bPlus;
	}

	public Long getAbPlus() {
		return abPlus;
	}

	public void setAbPlus(Long abPlus) {
		this.abPlus = abPlus;
	}

	public Long getaMinus() {
		return aMinus;
	}

	public void setaMinus(Long aMinus) {
		this.aMinus = aMinus;
	}

	public Long getoMinus() {
		return oMinus;
	}

	public void setoMinus(Long oMinus) {
		this.oMinus = oMinus;
	}

	public Long getbMinus() {
		return bMinus;
	}

	public void setbMinus(Long bMinus) {
		this.bMinus = bMinus;
	}

	public Long getAbMinus() {
		return abMinus;
	}

	public void setAbMinus(Long abMinus) {
		this.abMinus = abMinus;
	}
	

	
	
	
	
}
