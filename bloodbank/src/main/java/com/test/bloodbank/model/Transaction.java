package com.test.bloodbank.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;

import java.util.Date;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transId;
    private String bloodGroup;
    @Min(value = 1)
    private Long unitOfBloodGroup;
    private Date timeStamp;
    private String status;
    private String patient;
    private String cause;
    
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name= "userId", referencedColumnName="userId")
	private User user;
    
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name= "bloodBankId", referencedColumnName="bloodBankId")
	private BloodBank bloodBank;
	
    public Transaction() {}

	public Long getTransId() {
		return transId;
	}

	public void setTransId(Long transId) {
		this.transId = transId;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Long getUnitOfBloodGroup() {
		return unitOfBloodGroup;
	}

	public void setUnitOfBloodGroup(Long unitOfBloodGroup) {
		this.unitOfBloodGroup = unitOfBloodGroup;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BloodBank getBloodBank() {
		return bloodBank;
	}

	public void setBloodBank(BloodBank bloodBank) {
		this.bloodBank = bloodBank;
	}



	
	
    
    
}
