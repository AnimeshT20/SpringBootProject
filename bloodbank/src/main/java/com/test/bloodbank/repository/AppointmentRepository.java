package com.test.bloodbank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.bloodbank.model.Appointment;
import com.test.bloodbank.model.User;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long>{
	Appointment findByAppointmentId(Long appointmentId);
	List<Appointment> findByStatus(String status);
	List<Appointment> findByUser(User user);
}
