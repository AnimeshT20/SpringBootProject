package com.test.bloodbank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.bloodbank.model.BloodBank;

@Repository
public interface BloodBankRepository extends JpaRepository<BloodBank, Long>{
	BloodBank findByBloodBankId(Long bloodBankId);
	BloodBank findByBloodBankName(String bloodBankName);
	List<BloodBank> findByDistrictAndState(String district, String state);
	
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and a_plus > 0", nativeQuery=true)
	List<BloodBank> findBanksByAPlus(String state, String district);
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and a_minus > 0", nativeQuery=true)
	List<BloodBank> findBanksByAMinus(String state, String district);
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and b_plus > 0", nativeQuery=true)
	List<BloodBank> findBanksByBPlus(String state, String district);	
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and b_minus > 0", nativeQuery=true)
	List<BloodBank> findBanksByBMinus(String state, String district);	
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and o_plus > 0", nativeQuery=true)
	List<BloodBank> findBanksByOPlus(String state, String district);	
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and o_minus > 0", nativeQuery=true)
	List<BloodBank> findBanksByOMinus(String state, String district);	
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and ab_plus > 0", nativeQuery=true)
	List<BloodBank> findBanksByABPlus(String state, String district);
	@Query(value="Select * from blood_bank where state = ?1 and district = ?2 and ab_minus > 0", nativeQuery=true)
	List<BloodBank> findBanksByABMinus(String state, String district);
	
}
