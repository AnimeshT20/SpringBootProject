package com.test.bloodbank.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.test.bloodbank.dto.DonorDto;
import com.test.bloodbank.dto.ManageAppointmentDto;
import com.test.bloodbank.exceptions.EarlyDonationException;
import com.test.bloodbank.exceptions.InvalidAgeException;
import com.test.bloodbank.exceptions.InvalidWeightException;
import com.test.bloodbank.service.AppointmentService;
import com.test.bloodbank.service.BloodBankService;

@Controller
public class AppointmentController {
	
	@Autowired
	private AppointmentService appService;
	@Autowired
	private BloodBankService bankService;
	
	@GetMapping("/user/donate")
	public String donarDetails(Model model) {
		DonorDto donor = new DonorDto();
		model.addAttribute("donor", donor);
		model.addAttribute("banks", bankService.viewAllBloodBanks());
		return "schedule";
	}
	@PostMapping("/user/donate")
	public String donateBlood(@ModelAttribute("donor") DonorDto donorDto) throws ParseException {
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
				SecurityContextHolder.getContext().getAuthentication();
		try {
			appService.scheduleAppointment(auth.getName(), donorDto);
			return "redirect:/userhomepage?request";
		}catch(InvalidAgeException e) {
			return "redirect:/schedule?weight";
		}catch(InvalidWeightException e) {
			return "redirect:/schedule?weight";
		}catch(EarlyDonationException e) {
			return "redirect:/schedule?period";
		}
	}

	@GetMapping("/user/manage/{id}")
	public String manageDetails(Model model) {
		ManageAppointmentDto manage = new ManageAppointmentDto();
		model.addAttribute("banks", bankService.viewAllBloodBanks());
		model.addAttribute("manage", manage);
		return "manage/{id}";
	}
	@PostMapping("/user/manage/{id}")
	public String manageAppointment(@PathVariable("id")Long appointmentId, @ModelAttribute("manage") ManageAppointmentDto manageDto){
		
		appService.manageAppointment(appointmentId, manageDto);
		return "redirect:/userhomepage?app";
	}
	
	@GetMapping("/admin/approveapp/{id}")
	public String approveAppointment(@PathVariable("id")Long appId) {
		appService.approveAppointment(appId);
		return "adminhomepage";
	}
	@GetMapping("/admin/declineapp/{id}")
	public String declineAppointment(@PathVariable("id")Long appId) {
		appService.deniedAppointment(appId);
		return "adminhomepage";
	}
	
}
