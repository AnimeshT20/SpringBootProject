package com.test.bloodbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.test.bloodbank.dto.SeekerDto;
import com.test.bloodbank.service.BloodBankService;
import com.test.bloodbank.service.TransactionService;

@Controller
public class TransactionController {
	
	@Autowired
	private TransactionService transService;
	@Autowired
	private BloodBankService bankService;
	
	@GetMapping("/user/transaction")
	public String userTransactions(Model model) {
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
				SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("transaction",transService.viewAllTransactionByUser(auth.getName()));
		return "viewusertransaction";
	}
	
	@GetMapping("/admin/transaction")
	public String allTransactions(Model model) {
		model.addAttribute("transaction",transService.viewAllTransaction());
		return "viewalltransactions";
	}
	
	@GetMapping("/user/receive")
	public String seekDetails(Model model) {
		SeekerDto seekDto = new SeekerDto();
		model.addAttribute("request",seekDto);
		model.addAttribute("banks", bankService.viewAllBloodBanks());
		return "seek";
	}
	@PostMapping("/user/receive")
	public String seekBlood(@ModelAttribute("request") SeekerDto seekDto){
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
				SecurityContextHolder.getContext().getAuthentication();
		transService.seekBlood(auth.getName(), seekDto);
		return "redirect:/?request";
	}
	@GetMapping("/admin/approvetrans/{id}")
	public String approveTransaction(@PathVariable("id") Long transId) {
		transService.approveSeekRequest(transId);
		return "adminhomepage";
	}
	@GetMapping("/admin/declinetrans/{id}")
	public String declineTransaction(@PathVariable("id")Long transId) {
		transService.declineSeekRequest(transId);
		return "adminhomepage";
	}
	
}
