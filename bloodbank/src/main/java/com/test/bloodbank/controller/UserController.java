package com.test.bloodbank.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.test.bloodbank.dto.UpdateUserDto;
import com.test.bloodbank.dto.UserDto;
import com.test.bloodbank.exceptions.InvalidPasswordException;
import com.test.bloodbank.exceptions.UserExistException;
import com.test.bloodbank.model.User;
import com.test.bloodbank.service.AppointmentService;
import com.test.bloodbank.service.TransactionService;
import com.test.bloodbank.service.UserService;
import com.test.bloodbank.util.UtilConstants;

@Controller
public class UserController{
	
	@Autowired
	private UserService userService;
	@Autowired
	private AppointmentService appService;
	@Autowired
	private TransactionService tranService;
	
	@GetMapping("/login")
	public String getLoginDetails() {
		return "login";
	}
	
	@GetMapping("/")
	public String home(Model model) {
		User user =loginDetails();
		if(user.getRole().equals(UtilConstants.USER_ROLE_ADMIN))
		{	
			
			model.addAttribute("app",appService.showRequests());
			model.addAttribute("trans", tranService.showRequests() );
			return "adminhomepage";
		}
		else {
			model.addAttribute("app",appService.getAppointment(user));
			model.addAttribute("trans", tranService.showRequests() );
			return "userhomepage";
		}
	}
	
	@GetMapping("/user/registration")
	public String registerUserDetails(Model model) {

		UserDto user = new UserDto();
		model.addAttribute("user",user);
		return "register";
	}
	@PostMapping("/user/registration")
	public String registerUser(@Valid @ModelAttribute("user") UserDto userDto, BindingResult result ) throws UserExistException, InvalidPasswordException{
		try {
			
			if(result.hasErrors()) {
				return "register";
			}
			else if(userService.registerUser(userDto)!=null)
				return "redirect:/login?success";
			else return "redirect:/register?fail";
		}catch(UserExistException e) {
			return "redirect:/register?user";
		}catch(InvalidPasswordException e) {
			return "redirect:/register?pass";
		}
	}
	
	@GetMapping("/user/userProfile")
	public String showUserDetails(Model model) {
		User user = loginDetails();
		System.out.println(user.getFirstName() + "-------------------------------------------------");
		model.addAttribute("user", user );
		return "userprofile";
	}
	
	@GetMapping("/user/updateProfile")
	public String updateDetails(Model model) {
		UpdateUserDto userDto = new UpdateUserDto();
		model.addAttribute("user", loginDetails());
		model.addAttribute("update", userDto);
		return "updateuser";
	}
	
	@PostMapping("/user/updateProfile")
	public String updateUser(@ModelAttribute("update") UpdateUserDto userDto){
		if(userService.updateUser(loginDetails().getUsername(), userDto)!=null)
			return "redirect:/user/userProfile?success";
		else return "redirect:/user/updateProfile?fail";
	}

	@GetMapping("/admin/userProfiles")
	public String showAllUsers(Model model) {
		model.addAttribute("users", userService.showAllUsers());
		return "viewusers";
	}
	
	public User loginDetails() {
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)
				SecurityContextHolder.getContext().getAuthentication();	
		return userService.showUserdetails(auth.getName());
	}
	
	
	
	
	
}
