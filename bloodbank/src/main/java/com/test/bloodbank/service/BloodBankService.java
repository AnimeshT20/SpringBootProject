package com.test.bloodbank.service;

import java.util.List;

import com.test.bloodbank.dto.AddBloodBankDto;
import com.test.bloodbank.dto.BankSearchDto;
import com.test.bloodbank.dto.BloodSearchDto;
import com.test.bloodbank.dto.UpdateBankDto;
import com.test.bloodbank.model.BloodBank;

public interface BloodBankService {
	BloodBank addBloodBank(AddBloodBankDto bloodBankdto);
	List<BloodBank> viewAllBloodBanks();
	List<BloodBank> viewBloodBanksByLocation(BankSearchDto bankDto);
	BloodBank viewBloodBankByName(String bankName);
	void deleteBloodBank(Long bloodBankId);
	BloodBank updateBloodBank(Long bloodBankId, UpdateBankDto updateDto);
	List<BloodBank> getBankByBloodType(BloodSearchDto bloodDto);
}
