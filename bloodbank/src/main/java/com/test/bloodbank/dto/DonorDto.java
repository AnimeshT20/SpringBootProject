package com.test.bloodbank.dto;

public class DonorDto {
	private String date;
	private String time;
	private Integer age;
	private Double weight;
	private String bloodBankName;
	private String lastDonationDate;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getBloodBankName() {
		return bloodBankName;
	}
	public void setBloodBankName(String bloodBankName) {
		this.bloodBankName = bloodBankName;
	}
	public String getLastDonationDate() {
		return lastDonationDate;
	}
	public void setLastDonationDate(String lastDonationDate) {
		this.lastDonationDate = lastDonationDate;
	}

	
}
